package com.qwertovsky.reportgenerator.jasper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRCsvDataSource;

public class ReportDataSource
{
	private static Log log = LogFactory.getLog(ReportDataSource.class);
	
	private String name;
	private ReportDataSourceType type;
	
	private JRDataSource dataSource = null;
	private Connection dbConnection = null;
	
	//for CSV dataSource
	private String csvFileName;
	private String encoding;
	private char fieldDelimiter;
	private String recordDelimiter;
	private boolean isFirstRowAsHeader;
	
	//for connection
	private String dbDriverClassName;
	private String dbDriverPath;
	private String dbUrl;
	private String dbUser;
	private String dbPassword;
	
	//--------------------------------------------
	public ReportDataSource()
	{
		type = ReportDataSourceType.EMPTY;
	}
	
	//--------------------------------------------
	public ReportDataSource(String name, String csvFileName, String encoding
			, char fieldDelimiter, String recordDelimiter, boolean isFirstRowAsHeader)
	throws Exception
	{
		this.csvFileName = csvFileName;
		this.encoding = encoding;
		this.fieldDelimiter = fieldDelimiter;
		this.recordDelimiter = recordDelimiter;
		this.isFirstRowAsHeader = isFirstRowAsHeader;
		
		type = ReportDataSourceType.CSV;
		setName(name);
		
		try
		{
			createCSVDataSource();
		}   catch (Exception e)
		{
			String errorMessage = "Error get CSV data source: " + e.getMessage();
			log.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
	
	//--------------------------------------------
	public ReportDataSource(String name, String dbDriverName, String dbDriverPath
			, String dbUrl, String dbUser, String dbPassword)
	throws Exception
	{
		this.dbDriverClassName = dbDriverName;
		this.dbDriverPath = dbDriverPath;
		this.dbUrl = dbUrl;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;
		
		type = ReportDataSourceType.DB;
		setName(name);
		
		//init connection
		try
		{
			createConnection();
		} catch (Exception e)
		{
			String errorMessage = "Error create connection: " + e.getMessage();
			log.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
	
	//--------------------------------------------
	private void createCSVDataSource()
	throws UnsupportedEncodingException, FileNotFoundException
	{
		InputStreamReader csvReader = null;
		File csvFile = new File(csvFileName);
		csvReader = new InputStreamReader(
					new FileInputStream(csvFile), encoding);
		JRCsvDataSource ds = new JRCsvDataSource(csvReader);
		ds.setFieldDelimiter(fieldDelimiter);
		ds.setRecordDelimiter(recordDelimiter);
		ds.setUseFirstRowAsHeader(isFirstRowAsHeader);
		dataSource = ds;
	}
	
	//--------------------------------------------
	private void createConnection()
	throws MalformedURLException, ClassNotFoundException, SQLException
	{
		File jdbcFile = new File(dbDriverPath);
		URLClassLoader jdbcClassLoader = new URLClassLoader(new URL[]{jdbcFile.toURI().toURL()}
				, this.getClass().getClassLoader());
		Class.forName(dbDriverClassName, true, jdbcClassLoader);
		Properties prop = new Properties();
	    prop.setProperty("user", dbUser);
	    prop.setProperty("password", dbPassword); 
		dbConnection = DriverManager.getConnection(dbUrl, prop);
	}

	//--------------------------------------------
	protected Connection getConnection()
	throws Exception
	{
		if(dbConnection == null)
			try
			{
				createConnection();
			} catch (Exception e)
			{
				String errorMessage = "Error create connection: " + e.getMessage();
				log.error(errorMessage);
				throw new Exception(errorMessage);
			}
		return dbConnection;
	}
	
	//--------------------------------------------
	public JRDataSource getDataSource()
	throws Exception
	{
		if(dataSource == null)
		{
			if(type == ReportDataSourceType.CSV)
			{
				try
				{
					createCSVDataSource();
				}  catch (Exception e)
				{
					String errorMessage = "Error get CSV data source: " + e.getMessage();
					log.error(errorMessage);
					throw new Exception(errorMessage);
				}
			}
		}
		
		return dataSource;
	}
	
	//--------------------------------------------
	public void close() throws SQLException
	{
		if(dbConnection != null && !dbConnection.isClosed())
			dbConnection.close();
	}
	
	//--------------------------------------------
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setDbDriverPath(String dbDriverPath)
	{
		this.dbDriverPath = dbDriverPath;
	}

	public String getDbDriverPath()
	{
		return dbDriverPath;
	}
	
	public String getDbPassword()
	{
		return dbPassword;
	}

	public void setDbPassword(String dbPassword)
	{
		this.dbPassword = dbPassword;
	}

	public ReportDataSourceType getType()
	{
		return type;
	}

	

	public String getDbDriverClassName()
	{
		return dbDriverClassName;
	}

	public String getDbUrl()
	{
		return dbUrl;
	}

	public String getDbUser()
	{
		return dbUser;
	}
}
