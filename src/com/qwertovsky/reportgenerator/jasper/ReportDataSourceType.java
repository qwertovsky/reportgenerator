package com.qwertovsky.reportgenerator.jasper;

public enum ReportDataSourceType
{
	CSV, DB, EMPTY
}
