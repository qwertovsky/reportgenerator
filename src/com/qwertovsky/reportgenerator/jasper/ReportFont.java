package com.qwertovsky.reportgenerator.jasper;

import java.io.File;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class ReportFont
{
	@XmlAttribute(name = "name")
	private String fontFamilyName;
	@XmlElement(name="normal")
	private  String normalFontPath;
	@XmlElement(name="bold")
	private String boldFontPath;
	@XmlElement(name="italic")
	private String italicFontPath;
	@XmlElement(name="boldItalic")
	private String boldItalicFontPath;
	private String pdfEncoding = "Identity-H";
	private boolean pdfEmbedded = true;
	
	@SuppressWarnings("unused")
	private ReportFont()
	{
		
	}
	//--------------------------------------------
	public ReportFont(String name, String normalFontPath)
	{
		if(name == null || name.length() == 0)
			throw new IllegalArgumentException("Font name must not be null or empty");
		if(normalFontPath == null || normalFontPath.length() == 0)
			throw new IllegalArgumentException("Path to normal TrueType font must not be null");
		File file = new File(normalFontPath);
		if(!file.exists())
			throw new IllegalArgumentException("TrueType font file not exists");
		this.fontFamilyName = name;
		this.normalFontPath = normalFontPath;
	}
	
	//--------------------------------------------
	public String getFontFamilyName()
	{
		return fontFamilyName;
	}

	public void setFontFamilyName(String fontFamilyName)
	{
		this.fontFamilyName = fontFamilyName;
	}
	
	//--------------------------------------------
	public String getNormalFontPath()
	{
		return normalFontPath;
	}

	public void setNormalFontPath(String normalFontPath)
	{
		if(normalFontPath == null || normalFontPath.length() == 0)
			throw new IllegalArgumentException("Path to normal TrueType font must not be null");
		File file = new File(normalFontPath);
		if(!file.exists())
			throw new IllegalArgumentException("TrueType font file not exists");
		this.normalFontPath = normalFontPath;
	}
	
	//--------------------------------------------
	public String getBoldFontPath()
	{
		return boldFontPath;
	}

	public void setBoldFontPath(String boldFontPath)
	{
		if(boldFontPath == null || boldFontPath.length() == 0)
			throw new IllegalArgumentException("Path to TrueType font must not be null");
		File file = new File(boldFontPath);
		if(!file.exists())
			throw new IllegalArgumentException("TrueType font file not exists");
		this.boldFontPath = boldFontPath;
	}
	
	//--------------------------------------------
	public String getItalicFontPath()
	{
		return italicFontPath;
	}

	public void setItalicFontPath(String italicFontPath)
	{
		if(italicFontPath == null || italicFontPath.length() == 0)
			throw new IllegalArgumentException("Path to TrueType font must not be null");
		File file = new File(italicFontPath);
		if(!file.exists())
			throw new IllegalArgumentException("TrueType font file not exists");
		this.italicFontPath = italicFontPath;
	}

	//--------------------------------------------
	public String getBoldItalicFontPath()
	{
		return boldItalicFontPath;
	}

	public void setBoldItalicFontPath(String boldItalicFontPath)
	{
		if(boldItalicFontPath == null || boldItalicFontPath.length() == 0)
			throw new IllegalArgumentException("Path to TrueType font must not be null");
		File file = new File(boldItalicFontPath);
		if(!file.exists())
			throw new IllegalArgumentException("TrueType font file not exists");
		this.boldItalicFontPath = boldItalicFontPath;
	}
	
	//--------------------------------------------
	public String getPdfEncoding()
	{
		return pdfEncoding;
	}
	
	public void setPdfEncoding(String pdfEncoding)
	{
		this.pdfEncoding = pdfEncoding;
	}
	
	//--------------------------------------------
	public boolean isPdfEmbedded()
	{
		return pdfEmbedded;
	}

	public void setPdfEmbedded(boolean pdfEmbedded)
	{
		this.pdfEmbedded = pdfEmbedded;
	}

	
}
