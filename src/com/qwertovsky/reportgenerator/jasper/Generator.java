package com.qwertovsky.reportgenerator.jasper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRValidationFault;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Generator
{
	private static Log log = LogFactory.getLog(Generator.class);
	
	//--------------------------------------------
	public static byte[] compileToByteArray(String jrxmlFileName)
	throws Exception //file load, faults, compile
	{
		log.trace("Load JRXML file...");
		JasperDesign design = JRXmlLoader.load(jrxmlFileName);
		log.trace("Report design verification...");
		Collection<JRValidationFault> designFaults = JasperCompileManager.verifyDesign(design);
		if(!designFaults.isEmpty())
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Errors in report design:\n");
			for(JRValidationFault fault : designFaults)
			{
				stringBuilder.append(fault.getMessage() + "\n");
			}
			log.error(stringBuilder.toString());
			
			throw new JRException("Faults in report file: " + jrxmlFileName);
		}
		ByteArrayOutputStream reportOutputStream = new ByteArrayOutputStream();
		JasperCompileManager.compileReportToStream(design, reportOutputStream);
		log.info(jrxmlFileName + " has been compiled");
		byte[] report = reportOutputStream.toByteArray();
		return report;
	}
	
	//--------------------------------------------
	public static JasperReport compile(String jrxmlFileName)
	throws Exception //file load, faults, compile
	{
		log.trace("Load JRXML file...");
		JasperDesign design = JRXmlLoader.load(jrxmlFileName);
		log.trace("Report design verification...");
		Collection<JRValidationFault> designFaults = JasperCompileManager.verifyDesign(design);
		if(!designFaults.isEmpty())
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("Errors in report design:\n");
			for(JRValidationFault fault : designFaults)
			{
				stringBuilder.append(fault.getMessage() + "\n");
			}
			log.error(stringBuilder.toString());
			
			throw new JRException("Faults in report file: " + jrxmlFileName);
		}
		JasperReport report = JasperCompileManager.compileReport(design);
		log.info(jrxmlFileName + " has been compiled");
		return report;
	}
	
	//--------------------------------------------
	public static byte[] generateReportByteAray(InputStream jasperReport
			, Map<String, Object> parameters
			, ReportFormat format
			, ReportDataSource dataSource)
	throws Exception
	{
		//get type of source
		ReportDataSourceType dataSourceType = dataSource.getType();
		
		//fill report
		JasperPrint print = null;
		
		switch (dataSourceType)
		{
		case EMPTY:
			JRDataSource emptyDS = new JREmptyDataSource();
			print = JasperFillManager.fillReport(jasperReport
						, parameters
						, emptyDS);
			break;
		case CSV:
			//create JRCSVDataSource
			JRDataSource csvDS = dataSource.getDataSource();
			print = JasperFillManager.fillReport(jasperReport
						, parameters
						, csvDS);
			break;
		case DB:
			//create connection
			Connection dbConnection = dataSource.getConnection();
			
			print = JasperFillManager.fillReport(jasperReport
					, parameters
					, dbConnection);
			break;
		};
		
		byte[] report = exportToByteArray(print, format);
		return report;
	}
	
	//--------------------------------------------
	private static byte[] exportToByteArray(JasperPrint print
			, ReportFormat format)
	throws JRException
	{
		//export to file
		JRExporter exporter = null;
		switch (format)
		{
		case PDF:
			exporter = new JRPdfExporter();
			break;
		case XLSX:
			exporter = new JRXlsxExporter();
			break;
		}
		ByteArrayOutputStream reportOutputStream = new ByteArrayOutputStream();
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, reportOutputStream);
		exporter.exportReport();
		byte[] report = reportOutputStream.toByteArray();
		return report;
	}
}


