package com.qwertovsky.reportgenerator.jasper;

public enum ReportFormat {
	PDF, XLSX
}
