package com.qwertovsky.reportgenerator.jasper;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fontFamilies")
public class FontFamilies
{
	@XmlElement(name = "fontFamily")
	private Set<ReportFont> set = new HashSet<ReportFont>();
	
	public Set<ReportFont> getFamilies()
	{
		return set;
	}
	
	
}
