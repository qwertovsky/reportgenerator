package com.qwertovsky.reportgenerator.jasper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class FontsGenerator
{
	private static volatile FontsGenerator generator;
	private static String fontMapFilePath = "./fonts/fonts.xml";
	private Log log = LogFactory.getLog(FontsGenerator.class);
	private FontFamilies families;
	
	public static FontsGenerator getInstance()
	throws Exception
	{
		if(generator == null)
		{
			synchronized (FontsGenerator.class)
			{
				if(generator == null)
				{
					generator = new FontsGenerator();
					try
					{
						JAXBContext context = JAXBContext.newInstance(FontFamilies.class);
						Unmarshaller unmarshaller = context.createUnmarshaller();
						generator.families = (FontFamilies)unmarshaller.unmarshal(new File(fontMapFilePath));
					} catch (JAXBException e)
					{
						generator.families = new FontFamilies();
					}
				}
			}
			
		}
		return generator;
		
	}
	
	// --------------------------------------------
	public void addFont(ReportFont font) throws Exception
	{
		if(families.getFamilies().contains(font))
			throw new Exception("FontFamily with this name already exists");
		
		families.getFamilies().add(font);
		marshal();
	}
	
	// -------------------------------------------
	public void removeFont(ReportFont font)
	throws Exception
	{
		families.getFamilies().remove(font);
		marshal();
	}
	
	// -------------------------------------------
	public void removeAllFonts()
	throws Exception
	{
		families.getFamilies().clear();
		marshal();
	}
	
	// -------------------------------------------
	private void marshal()
	throws Exception
	{
		JAXBContext context = JAXBContext.newInstance(FontFamilies.class);
		Marshaller marshaller = context.createMarshaller();
		OutputFormat format = new OutputFormat();
		format.setLineWidth(80);
		format.setIndenting(true);
		format.setIndent(4);
		format.setCDataElements(new String[] { "^normal", "^bold", "^italic",
				"^boldItalic", "^pdfEncoding", "^pdfEmbedded", "^locale" });
		File fontMapFile = new File(fontMapFilePath);
		OutputStream os = new FileOutputStream(fontMapFile);
		XMLSerializer s = new XMLSerializer(os, format);
		try
		{
			marshaller.marshal(families, s.asContentHandler());
		} catch (Exception e)
		{
			String error = "Error write fontMap: " + e.getMessage();
			log.error(error);
			throw new Exception(error);
		} finally
		{
			os.close();
		}
	}
}
