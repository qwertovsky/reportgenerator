package com.qwertovsky.reportgenerator.jasper;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ReportGeneratorTest
{

	@Test
	public void testGenerate() throws Exception
	{
		//TODO check classpath jasperreperts_extension.properties
		
		String familyName = "Arial";
		String normalFontFile = "fonts/arial.ttf";
		String boldFontFile = "fonts/arialbd.ttf";
		String italicFontFile = "fonts/ariali.ttf";
		String boldItalicFontFile = "fonts/arialbi.ttf";
		ReportFont font = new ReportFont(familyName, normalFontFile);
		font.setBoldFontPath(boldFontFile);
		font.setItalicFontPath(italicFontFile);
		font.setBoldItalicFontPath(boldItalicFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);
		
		ReportDataSource dsFactura = new ReportDataSource("facturaDS",
				"test/sf_cor_test.csv", "windows-1251",
				';', "\r\n", true);
		ReportDataSource dsCorrFactura = new ReportDataSource("facturaCorrDS",
				"test/sf_cor_test.csv", "windows-1251",
				';', "\r\n", true);
		ReportDataSource dsEmpty = new ReportDataSource();
		byte[] reportBA = null;
		reportBA = Generator
				.compileToByteArray("test/factura_full.jrxml");

		Object factura = Generator
				.compile("test/factura.jrxml");
		Object facturaCorr = Generator
				.compile("test/factura_cor.jrxml");

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("factursDS", dsFactura.getDataSource());
		parameters.put("factursCorrDS", dsCorrFactura.getDataSource());
		parameters.put("factursReport", factura);
		parameters.put("factursCorrReport", facturaCorr);

		byte[] reportPdfBA = Generator.generateReportByteAray(
				new ByteArrayInputStream(reportBA), parameters,
				ReportFormat.PDF, dsEmpty);
		FileOutputStream pdfOS = new FileOutputStream("factura.pdf");
		pdfOS.write(reportPdfBA);
		pdfOS.flush();
		pdfOS.close();
	}
}
