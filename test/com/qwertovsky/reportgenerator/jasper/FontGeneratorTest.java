package com.qwertovsky.reportgenerator.jasper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class FontGeneratorTest
{
	File fontsFile = new File("fonts/fonts.xml");
	String familyName = "Arial";
	String normalFontFile = "fonts/arial.ttf";
	String boldFontFile = "fonts/arialbd.ttf";
	String italicFontFile = "fonts/ariali.ttf";
	String boldItalicFontFile = "fonts/arialbi.ttf";

	@Before
	public void setUp() throws Exception
	{
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.removeAllFonts();
		
	}

	// -------------------------------------------
	@Test
	public void testAddFont() throws Exception
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		font.setBoldFontPath(boldFontFile);
		font.setItalicFontPath(italicFontFile);
		font.setBoldItalicFontPath(boldItalicFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);

		// check content
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setCoalescing(true);
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(fontsFile);
		Element rootElement = doc.getDocumentElement();
		NodeList families = rootElement.getElementsByTagName("fontFamily");
		Element family = (Element) families.item(0);
		assertEquals("Not exists family " + familyName, familyName,
				family.getAttribute("name"));
		try
		{
			Element fontElement = (Element) family.getElementsByTagName(
					"normal").item(0);
			assertEquals("Wrong normal font", normalFontFile,
					fontElement.getTextContent());
		} catch (NullPointerException e)
		{
			fail("Normal font not exists");
		}

		try
		{
			Element fontElement = (Element) family.getElementsByTagName("bold")
					.item(0);
			assertEquals("Wrong bold font", boldFontFile,
					fontElement.getTextContent());
		} catch (NullPointerException e)
		{
			fail("Bold font not exists");
		}

		try
		{
			Element fontElement = (Element) family.getElementsByTagName(
					"italic").item(0);
			assertEquals("Wrong italic font", italicFontFile,
					fontElement.getTextContent());
		} catch (NullPointerException e)
		{
			fail("Italic font not exists");
		}

		try
		{
			Element fontElement = (Element) family.getElementsByTagName(
					"boldItalic").item(0);
			assertEquals("Not exists bold-italic font", boldItalicFontFile,
					fontElement.getTextContent());
		} catch (NullPointerException e)
		{
			fail("Bold-italic font not exists");
		}
	}

	// -------------------------------------------
	@Test
	public void testAddFontOnlyNormal() throws Exception
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);

		// check content (only normal font node)
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setCoalescing(true);
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(fontsFile);
		Element rootElement = doc.getDocumentElement();
		NodeList families = rootElement.getElementsByTagName("fontFamily");
		Element family = (Element) families.item(0);
		assertEquals("Not exists family " + familyName, familyName,
				family.getAttribute("name"));
		Element fontElement = (Element) family.getElementsByTagName("normal")
				.item(0);
		assertEquals("Wrong normal font", normalFontFile,
				fontElement.getTextContent());
		fontElement = (Element) family.getElementsByTagName("bold")
				.item(0);
		assertNull("Bold font must not exists", fontElement);
		fontElement = (Element) family.getElementsByTagName("italic")
				.item(0);
		assertNull("Italic font must not exists", fontElement);

		fontElement = (Element) family.getElementsByTagName(
				"boldItalic").item(0);
		assertNull("Bold-italic font must not exists", fontElement);
	}

	// -------------------------------------------
	@Test
	public void testAddFontErrorPath() throws Exception
	{
		try
		{
			new ReportFont(familyName, "fonts/arialnormal.ttf");
			fail("Not exits file for normal font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			new ReportFont(familyName, "");
			fail("Empty string for normal font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			new ReportFont(familyName, null);
			fail("Null for normal font is not good");
		} catch (Exception e)
		{
			// OK
		}

	}

	// -------------------------------------------
	@Test
	public void testSetBoldFont()
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		try
		{
			font.setBoldFontPath("fonts/arialbold.ttf");
			fail("Not exits file for bold font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setBoldFontPath("");
			fail("Empty path for bold font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setBoldFontPath(null);
			fail("Null path for bold font is not good");
		} catch (Exception e)
		{
			// OK
		}

	}

	// -------------------------------------------
	@Test
	public void testSetItalicFont()
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		try
		{
			font.setItalicFontPath("fonts/arialitalic.ttf");
			fail("Not exits file for italic font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setItalicFontPath("");
			fail("Empty path for italic font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setItalicFontPath(null);
			fail("Null path for italic font is not good");
		} catch (Exception e)
		{
			// OK
		}

	}

	// -------------------------------------------
	@Test
	public void testSetBoldItalicFont()
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		try
		{
			font.setBoldItalicFontPath("fonts/arialbolditalic.ttf");
			fail("Not exits file for bold-italic font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setBoldItalicFontPath("");
			fail("Empty path for bold-italic font is not good");
		} catch (Exception e)
		{
			// OK
		}
		try
		{
			font.setBoldItalicFontPath(null);
			fail("Null path for bold-italic font is not good");
		} catch (Exception e)
		{
			// OK
		}

	}

	// -------------------------------------------
	@Test(expected = Exception.class)
	public void testAddFontAlreadyExists() throws Exception
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);
		fontsGenerator.addFont(font);
	}
	
	// -------------------------------------------
	@Test
	public void testAddTwoFonts() throws Exception
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);
		ReportFont font2 = new ReportFont(familyName + 2, normalFontFile);
		fontsGenerator.addFont(font2);
		
		// check content
		JAXBContext context = JAXBContext.newInstance(FontFamilies.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		FontFamilies families = (FontFamilies)unmarshaller.unmarshal(fontsFile);
		Set<ReportFont> set = families.getFamilies();
		assertEquals("Counts of fonts must be 2", 2, set.size());
	}

	// -------------------------------------------
	@Test
	public void testRemoveFonts() throws Exception
	{
		ReportFont font = new ReportFont(familyName, normalFontFile);
		FontsGenerator fontsGenerator = FontsGenerator.getInstance();
		fontsGenerator.addFont(font);
		ReportFont font2 = new ReportFont(familyName + 2, normalFontFile);
		fontsGenerator.addFont(font2);
		fontsGenerator.removeFont(font2);
		
		// check content
		JAXBContext context = JAXBContext.newInstance(FontFamilies.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		FontFamilies families = (FontFamilies)unmarshaller.unmarshal(fontsFile);
		Set<ReportFont> set = families.getFamilies();
		assertEquals("Counts of fonts must be 1", 1, set.size());
	}
}
